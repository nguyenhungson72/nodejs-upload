const express = require('express');
const multer = require('multer');
const cors = require('cors');
const promiseFtp = require('promise-sftp');
const fs = require('fs');
const app = express();
const port = 4001;

const storage = multer.diskStorage({
    destination: './files',
    filename(req, file, cb) {
        const currTime = new Date().getTime();
        cb(null, `${currTime}_${file.originalname}`);
    }
});

const upload = multer({ storage });

app.use(cors());

app.post('/upload', upload.single('file'), (req, res) => {
    req.setTimeout(0);
    const file = req.file;
    const fileName = req.body.name;
    const userId = req.body.userId;
    const ftp = new promiseFtp();
    ftp.connect({ 
        host: 'ftp.espx.cloud', 
        user: 'espxvod', 
        password: 'daveAndStuart!' 
    })
        .then(function (serverMessage) {
            console.log('Check folder');
            return ftp.opendir(userId);
        })
        .catch((e) => {
            console.log('Create folder');
            return ftp.mkdir(userId);
        })
        .then((rss) => {
            console.log('FTP File to folder');
            return ftp.put(file.path, userId + '/' + fileName);
        })
        .then(function (mess) {
            res.send('1');
            fs.unlinkSync('./files/' + file.filename);
            return ftp.end();
        });
});

app.listen(port, () => console.log(`Example app listening on port ${port}!`));